import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SeparaVNPipe } from './@core/pipes/venta-nueva/separa-vn.pipe';
import { SeparaGMMPipe } from './@core/pipes/venta-nueva/separa-gmm.pipe';
import { SeparaPeruPipe } from './@core/pipes/venta-nueva/separa-peru.pipe';

@NgModule({
  declarations: [AppComponent, SeparaVNPipe, SeparaGMMPipe, SeparaPeruPipe],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
