import { Component, OnInit, NgZone } from '@angular/core';
import { UsuarioService } from '../../@core/services/usuario.service';
import Swal from 'sweetalert2';

import { ChildActivationStart, Router } from '@angular/router';
declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  generalData: any;
  public auth2: any;
  acceso: boolean;
  picture: string;

  constructor(
    private router: Router,
    private usuarioService: UsuarioService,
    private ngZone: NgZone
  ) {
    this.acceso = JSON.parse(localStorage.getItem('acceso'));
    if (this.acceso) {
      this.router.navigateByUrl('/modulos/dashboard');
    }
  }

  ngOnInit(): void {
    this.renderButton();
  }

  signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }

  onFailure(error) {
    console.log(error);
  }
  renderButton() {
    gapi.signin2.render('my-signin2', {
      scope: 'profile email',
      width: 'auto',
      height: 50,
      longtitle: true,
      theme: 'dark',
    });

    this.startApp();
  }

  startApp() {
    gapi.load('auth2', () => {
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      this.auth2 = gapi.auth2.init({
        client_id:
          '1047898691737-jc2pdn07g0c5i0t13ijuk3igdl4qhrup.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      this.attachSignin(document.getElementById('my-signin2'));
    });
  }

  attachSignin(element) {
    this.auth2.attachClickHandler(
      element,
      {},
      (googleUser) => {
        const id_token = googleUser.getAuthResponse().id_token;
        this.usuarioService.verificaToken(id_token).subscribe((data) => {
          if (data.acceso) {
            this.acceso = data.acceso;
            this.picture = data.picture;
            this.ngZone.run(() => {
              localStorage.setItem('acceso', JSON.stringify(this.acceso));
              localStorage.setItem('picture', JSON.stringify(this.picture));
              this.router.navigateByUrl('/modulos/dashboard');
            });
          } else {
            Swal.fire({
              title: 'No tienes acceso',
              icon: 'error',
            });
          }
        });
      },
      (error) => {
        alert(JSON.stringify(error, undefined, 2));
      }
    );
  }
}
