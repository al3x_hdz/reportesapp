import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/@theme/shared/shared.module';
import { LoginComponent } from './login/login.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [LoginComponent, ToolbarComponent],
  imports: [CommonModule, SharedModule, RouterModule],
  exports: [ToolbarComponent],
})
export class ThemeModule {}
