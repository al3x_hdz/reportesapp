import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-mx' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class CalendarioComponent implements OnInit {
  @Output() fechasSeleccionadas = new EventEmitter<any>();
  @Output() descargaHabilitada = new EventEmitter<boolean>();

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  constructor() {}

  ngOnInit(): void {}

  generaReporte() {
    if (this.range.value.start == null || this.range.value.end == null) {
      alert('Selecciones rango de fechas para generar el reporte.');
      return;
    }
    let fechaInicio = `${this.range.value.start._i.year}-${
      this.range.value.start._i.month + 1
    }-${this.range.value.start._i.date}`;
    let fechaFinal = `${this.range.value.end._i.year}-${
      this.range.value.end._i.month + 1
    }-${this.range.value.end._i.date}`;

    let fechas = {
      fechaIn: fechaInicio,
      fechaFin: fechaFinal,
    };

    this.fechasSeleccionadas.emit(fechas);
    this.descargaHabilitada.emit(true);
  }
}
