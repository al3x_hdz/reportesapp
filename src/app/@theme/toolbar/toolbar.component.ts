import {
  Component,
  Input,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { MatDrawer } from '@angular/material/sidenav';
import { UsuarioService } from 'src/app/@core/services/usuario.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @ViewChild('drawer') drawer: MatDrawer;

  titulo: string;
  picture: string;

  menuItemsController: Array<{
    destinationName: string;
    url: string;
    icon: string;
  }>;

  constructor(private router: Router, private usuarioService: UsuarioService) {
    this.menuItemsController = [
      {
        destinationName: 'Inicio',
        url: '/modulos/dashboard',
        icon: 'home',
      },
      {
        destinationName: 'Telefonía',
        url: '/modulos/telefonia',
        icon: 'call',
      },
      {
        destinationName: 'Venta Nueva',
        url: '/modulos/venta-nueva',
        icon: 'swap_horiz',
      },
    ];
    this.picture = JSON.parse(localStorage.getItem('picture'));
  }

  close(destinationName: string) {
    this.drawer.close();
    destinationName !== 'Inicio'
      ? (this.titulo = destinationName)
      : (this.titulo = '');
  }

  ngOnInit(): void {
    let miRuta = this.router.url;
    this.menuItemsController.map((element) => {
      if (element.url === miRuta && miRuta !== '/modulos/dashboard') {
        this.titulo = element.destinationName;
      }
    });
  }

  redirectDashboard(): any {
    this.router.navigate(['modulos/dashboard']);
    this.titulo = '';
  }

  cerrarSesion(): any {
    Swal.fire({
      title: 'DESEAS CERRAR SESIÓN?',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonText: 'NO',
      confirmButtonText: 'SI',
      cancelButtonColor: 'red',
      confirmButtonColor: 'green',
    }).then((resp) => {
      if (resp.value) {
        this.usuarioService.logout();
        Swal.fire({
          title: 'SESIÓN CERRADA',
          icon: 'success',
        });
      }
    });
  }
}
