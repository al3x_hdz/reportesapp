import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  ipAddress = '';
  acceso: boolean;

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.acceso = JSON.parse(localStorage.getItem('acceso'));
    if (this.acceso) {
      return true;
    }

    return false;
  }
}
