import { Pipe, PipeTransform } from '@angular/core';
import { Campanna } from 'src/app/@core/models/Venta-Nueva/Campanna';

@Pipe({
  name: 'separaVN',
})
export class SeparaVNPipe implements PipeTransform {
  transform(campannas: Campanna[]): Campanna[] {
    let aux = [];
    campannas.map((campanna) => {
      if (
        campanna.tipoContacto !== 'GASTOS MEDICOS MAYORES' &&
        campanna.tipoContacto !== 'PERU'
      ) {
        aux.push(campanna);
      }
    });
    return aux;
  }
}
