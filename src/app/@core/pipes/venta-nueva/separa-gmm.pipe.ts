import { Pipe, PipeTransform } from '@angular/core';
import { Campanna } from '../../models/Venta-Nueva/Campanna';

@Pipe({
  name: 'separaGMM',
})
export class SeparaGMMPipe implements PipeTransform {
  transform(campannas: Campanna[]): Campanna[] {
    let aux = [];
    campannas.map((campanna) => {
      if (campanna.tipoContacto === 'GASTOS MEDICOS MAYORES') {
        aux.push(campanna);
      }
    });

    return null;
  }
}
