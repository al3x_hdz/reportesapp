import { Pipe, PipeTransform } from '@angular/core';
import { Campanna } from '../../models/Venta-Nueva/Campanna';

@Pipe({
  name: 'separaPeru',
})
export class SeparaPeruPipe implements PipeTransform {
  transform(campannas: Campanna[]): Campanna[] {
    let aux = [];
    campannas.map((campanna) => {
      if (campanna.tipoContacto === 'PERU') {
        aux.push(campanna);
      }
    });

    return aux;
  }
}
