import { Pipe, PipeTransform } from '@angular/core';
import { LlamadasEntrantes } from 'src/app/@core/models/LlamadasEntrantes';

@Pipe({
  name: 'extraerLlamadasDelMes',
})
export class ExtraerLlamadasDelMesPipe implements PipeTransform {
  /**
   * Extrae las llamadas que coinciden con el mes de fechaActual
   * @param llamadasFiltradas : LlamadasEntrantes[]
   * @param fechaActual: Date
   * @returns llamadasEntrantes[]
   */
  transform(
    llamadasFiltradas: LlamadasEntrantes[],
    fechaActual: Date
  ): LlamadasEntrantes[] {
    let fecha;
    let aux = [];
    for (const iterator of llamadasFiltradas) {
      fecha = new Date(iterator.fechaGen);
      if (fecha.getMonth() == fechaActual.getMonth()) {
        aux.push(iterator);
      }
    }
    return aux;
  }
}
