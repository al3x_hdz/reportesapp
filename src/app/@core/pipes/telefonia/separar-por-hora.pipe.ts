import { Pipe, PipeTransform } from '@angular/core';
import { LlamadasEntrantes } from 'src/app/@core/models/LlamadasEntrantes';

@Pipe({
  name: 'separarPorHora',
})
export class SepararPorHoraPipe implements PipeTransform {
  /**
   * Separa las llamadas en un arreglo con espacios iguales a (hora actual - 8am)
   * se toma en cuenta que a las 22hrs ya no se registra nada
   *
   * @param llamadasFiltradas: LlamadasEntrantes[]
   * @param fechaActual: Date
   * @returns llamadasDeHoy[[]] separadas por hora
   */
  transform(
    llamadasFiltradas: LlamadasEntrantes[],
    fechaActual: Date
  ): LlamadasEntrantes[] {
    let fecha;
    let aux = [];
    let llamadasDeHoy = [];

    for (let i = 8; i <= fechaActual.getHours() && i <= 22; i++) {
      aux = [];
      for (const iterator of llamadasFiltradas) {
        fecha = new Date(iterator.fechaGen);
        if (
          fecha.getDate() === fechaActual.getDate() &&
          fecha.getHours() === i
        ) {
          aux.push(iterator);
        }
      }
      llamadasDeHoy[i] = aux;
    }
    return llamadasDeHoy;
  }
}
