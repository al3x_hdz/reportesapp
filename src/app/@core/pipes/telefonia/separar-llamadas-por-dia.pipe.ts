import { Pipe, PipeTransform } from '@angular/core';
import { LlamadasEntrantes } from 'src/app/@core/models/LlamadasEntrantes';

@Pipe({
  name: 'separarLlamadasPorDia',
})
export class SepararLlamadasPorDiaPipe implements PipeTransform {
  /**
   * Separa todas las llamadas por día, se toma en cuenta
   * que solo las del mes. Genera un arreglo con dimension equivalente a
   * los días que han transcurrido en el mes
   * @param llamadasFiltradas: LalamadasEntrantes[]
   * @param fechaActual: Date
   * @returns array[[]] separadas por días
   */
  transform(
    llamadasFiltradas: LlamadasEntrantes[],
    fechaActual: Date
  ): LlamadasEntrantes[] {
    let llamadasPorDia = [];

    for (let i = 1; i <= fechaActual.getDate(); i++) {
      llamadasPorDia[i - 1] = this.extraLlamadasDelDia(i, llamadasFiltradas);
    }
    return llamadasPorDia;
  }

  /**
   * Verifica qué llamadas coinciden con dia y los agrega a un
   * arreglo aux
   * @param dia: number
   * @param llamadasFiltradas: LlamadasEntrantes[]
   * @returns llamadasEntrantes[]
   */
  extraLlamadasDelDia(dia: number, llamadasFiltradas: LlamadasEntrantes[]) {
    let fecha;
    let aux = [];

    for (const iterator of llamadasFiltradas) {
      fecha = new Date(iterator.fechaGen);
      if (fecha.getDate() === dia) {
        aux.push(iterator);
      }
    }
    return aux;
  }
}
