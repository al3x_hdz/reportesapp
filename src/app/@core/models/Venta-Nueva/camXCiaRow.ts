export interface camXCiaRow {
  campanaGral: string;
  outLead: number;
  outNpc: number;
  outConv: number;
  telLead: number;
  telNpc: number;
  telConv: number;
  onlLead: number;
  inLead: number;
  inNpc: number;
  inConv: number;
  onlNpc: number;
  onlConv: number;
  gralLead: number;
  gralNpc: number;
  gralConv: number;
  pCobrada: number;
}
