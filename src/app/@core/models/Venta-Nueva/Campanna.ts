export interface Campanna {
  tipoSubarea: string;
  compannia: string;
  tipoContacto: string;
  contactosProd: number;
  npc: number;
  conversion: number;
  pCobrada: number;
}
