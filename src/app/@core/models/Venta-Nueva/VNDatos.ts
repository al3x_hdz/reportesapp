export interface VNDatos {
  leads: number | string;
  ventas: number | string;
  conversion: number | string;
}
