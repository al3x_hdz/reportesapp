import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const base_url = environment.baseURL;

@Injectable({
  providedIn: 'root',
})
export class CampannaService {
  constructor(private http: HttpClient) {}

  getCampannas(fechaIn: string, fechaFin: string) {
    try {
      return this.http.get(`${base_url}/venta-nueva/${fechaIn}/${fechaFin}`);
    } catch (error) {
      return error;
    }
  }
}
