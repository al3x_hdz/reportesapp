import { TestBed } from '@angular/core/testing';

import { CampannaService } from './campanna.service';

describe('CampannaService', () => {
  let service: CampannaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampannaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
