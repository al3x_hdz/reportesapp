import * as XLSX from 'xlsx';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class XlsxService {
  constructor() {}

  imprimirXLS(data: any, headers: string[], title: string) {
    const maxFilasPorHoja = 64000;
    let pagina = null;
    const hojaDeCalculo = XLSX.utils.book_new();
    const paginas = Math.floor(data.length / maxFilasPorHoja) + 1;
    for (let i = 0; i < paginas; i++) {
      pagina = XLSX.utils.json_to_sheet(
        data.slice(i * maxFilasPorHoja, (i + 1) * maxFilasPorHoja),
        // Lista de columnas a incluir en el archivo de Excel
        {
          header: headers,
        }
      );
      XLSX.utils.book_append_sheet(hojaDeCalculo, pagina, 'Hoja_' + (i + 1));
    }
    // construye el nombre del archivo, recuerda que no todos los componentes tienen titulo component
    let nombreArchivo = (title + '.xls').toLocaleLowerCase();
    nombreArchivo = nombreArchivo.replace(/ /g, '-').replace(/:/g, '-');

    XLSX.writeFile(
      hojaDeCalculo,
      nombreArchivo || 'SheetJSTableExport.' + 'xls'
    );
  }
}
