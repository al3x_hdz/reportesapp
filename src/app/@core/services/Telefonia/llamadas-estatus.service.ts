import { Injectable } from '@angular/core';
import { LlamadasEntrantes } from '../../models/LlamadasEntrantes';

@Injectable({
  providedIn: 'root',
})
export class LlamadasEstatusService {
  constructor() {}

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Abandonada'
   */
  llamadasAbandonadas(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter((llamada) => llamada.estatus == 'Abandonada');
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Contestada no Finalizada'
   */
  llamadasContestadas(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter((llamada) => llamada.estatus == 'Contestada');
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Contestada no Finalizada'
   */
  llamadasContestadasNoFinalizadas(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter(
      (llamada) => llamada.estatus == 'Contestada no finalizada'
    );
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Fuera de Horario'
   */
  llamadasFueraHorario(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter((llamada) => llamada.estatus == 'Fuera de Horario');
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'IVR'
   */
  ivr(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter((llamada) => llamada.estatus == 'IVR');
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Transferida a ATC'
   */
  tranferidaATC(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter((llamada) => llamada.estatus == 'Transferida a ATC');
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Transferida a ATC'
   */
  tranferidaPagos(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter(
      (llamada) => llamada.estatus == 'Transferida a Pagos'
    );
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Transferida a Renovaciones'
   */
  tranferidaRenovaciones(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter(
      (llamada) => llamada.estatus == 'Transferida a Renovaciones'
    );
  }

  /**
   * @param llamadas: arreglo que contiene todas las llamadas del reporte
   * @returns regresa las llamadas con estatus 'Transferida a Siniestros'
   */
  tranferidaSiniestros(llamadas: LlamadasEntrantes[]) {
    return llamadas.filter(
      (llamada) => llamada.estatus == 'Transferida a Siniestros'
    );
  }
}
