import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const base_url = environment.baseURL;

@Injectable({
  providedIn: 'root',
})
export class LlamadasEntrantesService {
  constructor(private http: HttpClient) {}

  getLlamadasEntrantes() {
    try {
      return this.http.get(`${base_url}/llamadas-entrantes`);
    } catch (error) {
      return error;
    }
  }

  getLlamadasEntrantesMes(mes: number, annio: number) {
    try {
      return this.http.get(
        `${base_url}/llamadas-entrantes/${mes + 1}/${annio}`
      );
    } catch (error) {
      return error;
    }
  }
}
