import { Injectable, NgZone } from '@angular/core';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const base_url = environment.baseURL;
import { Usuario } from '../models/Usuario';
import { promise } from 'selenium-webdriver';
declare const gapi: any;

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  public auth2: any;
  public usuario: Usuario;

  constructor(
    private http: HttpClient,
    private router: Router,
    private ngZone: NgZone
  ) {
    //this.googleInit();
  }

  verificaToken(token: string): Observable<any> {
    return this.http.post<any>(`${base_url}/usuarios/google/login`, { token });
  }
  // get token(): string {
  //   return localStorage.getItem('token') || '';
  // }

  // get role(): 'ADMIN_ROLE' | 'USER_ROLE' {
  //   return this.usuario.role;
  // }

  // get uid(): string {
  //   return this.usuario.uid || '';
  // }

  // get headers() {
  //   return {
  //     headers: {
  //       'x-token': this.token,
  //     },
  //   };
  // }

  // googleInit() {
  //   return new Promise((resolve) => {
  //     gapi.load('auth2', () => {
  //       this.auth2 = gapi.auth2.init({
  //         client_id:
  //           '538595308019-snnda4jbi0ljg3klljl21gm5o0ccu1rf.apps.googleusercontent.com',
  //         cookiepolicy: 'single_host_origin',
  //       });

  //       // resolve();
  //     });
  //   });
  // }

  // guardarLocalStorage(token: string) {
  //   localStorage.setItem('token', token);
  // }

  logout() {
    // localStorage.removeItem('token');
    // localStorage.removeItem('menu');
    try {
      localStorage.removeItem('acceso');
      localStorage.removeItem('picture');
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(() => {
        this.ngZone.run(() => {
          this.router.navigateByUrl('/login');
        });
      });
    } catch (error) {
      this.router.navigateByUrl('/login');
    }
    // this.auth2.signOut().then(() => {
    //   this.ngZone.run(() => {
    //     this.router.navigateByUrl('/login');
    //   });
    // });
  }

  // validarToken(): Observable<boolean> {
  //   return this.http
  //     .get(`${base_url}/login/renew`, {
  //       headers: {
  //         'x-token': this.token,
  //       },
  //     })
  //     .pipe(
  //       map((resp: any) => {
  //         const { email, google, nombre, img = '', uid } = resp.usuario;
  //         this.usuario = new Usuario(nombre, email, '', img, google, uid);

  //         this.guardarLocalStorage(resp.token);

  //         return true;
  //       }),
  //       catchError((error) => of(false))
  //     );
  // }

  // loginGoogle(token) {
  //   return this.http.post(`${base_url}/login/google`, { token }).pipe(
  //     tap((resp: any) => {
  //       this.guardarLocalStorage(resp.token);
  //     })
  //   );
  // }
}
