import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@theme/shared/shared.module';

import { ModulosRoutingModule } from './modulos-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ThemeModule } from '../@theme/@theme.module';
import { ModulosComponent } from './modulos.component';
import { TelefoniaComponent } from './telefonia/telefonia.component';
import { InboundModule } from './telefonia/inbound/inbound.module';
import { VentaNuevaComponent } from './venta-nueva/venta-nueva.component';
import { TablaComponent } from './venta-nueva/components/tabla/tabla.component';
import { CalendarioComponent } from '../@theme/calendario/calendario.component';
import { CampannasComponent } from './venta-nueva/components/campannas/campannas.component';
import { CamxciaComponent } from './venta-nueva/components/camxcia/camxcia.component';

@NgModule({
  declarations: [
    DashboardComponent,
    TelefoniaComponent,
    VentaNuevaComponent,
    ModulosComponent,
    TablaComponent,
    CalendarioComponent,
    CampannasComponent,
    CamxciaComponent,
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    SharedModule,
    ThemeModule,
    InboundModule,
  ],
})
export class ModulosModule {}
