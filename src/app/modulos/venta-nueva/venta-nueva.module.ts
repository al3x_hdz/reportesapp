import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentaNuevaRoutingModule } from './venta-nueva-routing.module';
@NgModule({
  declarations: [],
  imports: [CommonModule, VentaNuevaRoutingModule],
})
export class VentaNuevaModule {}
