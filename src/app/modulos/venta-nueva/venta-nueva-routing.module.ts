import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VentaNuevaComponent } from './venta-nueva.component';

const routes: Routes = [
  {
    path: '',
    component: VentaNuevaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VentaNuevaRoutingModule {}
