import { Component, OnInit } from '@angular/core';
import { CampannaService } from 'src/app/@core/services/Venta-Nueva/campanna.service';
import { camXCiaRow } from 'src/app/@core/models/Venta-Nueva/camXCiaRow';
import { XlsxService } from 'src/app/@core/services/Venta-Nueva/xlsx.service';
import { Campanna } from 'src/app/@core/models/Venta-Nueva/Campanna';

@Component({
  selector: 'app-venta-nueva',
  templateUrl: './venta-nueva.component.html',
  styleUrls: ['./venta-nueva.component.css'],
})
export class VentaNuevaComponent implements OnInit {
  loader: boolean = false;
  formateador = new Intl.NumberFormat('en-US');
  descargaHabilitada: boolean = false;

  fechaInicio: string;
  fechaFin: string;

  columnas = ['leads', 'ventas', 'conversion'];
  titulosCoulumasVersion1 = ['Leads', 'Ventas', 'Conversión'];
  titulosCoulumasVersion2 = ['Llamadas', 'Ventas', 'Conversión'];
  titulosCoulumasVersion3 = ['Leads y Llamadas', 'Ventas', 'Conversión'];
  titulosCoulumasVersion4 = ['Leads 2a', 'Ventas', 'Conversión'];

  outbound: any[];
  interaccionesOnline: any[];
  interaccionesTelefonica: any[];
  inbound: any[];
  otros: any[];

  gastosMedicos: any[];
  peru: any[];
  fullOnline: any[];
  vacio: any[];

  mejorPrecio: any[];
  cobranza: any[];
  retencion: any[];

  subTotal: any[];
  totalGeneral: any[];

  camXCia: camXCiaRow[] = [];

  campannas: Campanna[];

  constructor(
    private campanniaService: CampannaService,
    private xlsxService: XlsxService
  ) {}

  ngOnInit(): void {
    this.reiniciaVariables();
  }

  leeFechasSeleccionadas(fechasSeleccionadas: any) {
    this.reiniciaVariables();
    this.loader = true;
    let fechaI = new Date(fechasSeleccionadas.fechaIn);
    let fechaF = new Date(fechasSeleccionadas.fechaFin);

    this.fechaInicio = `${fechaI.getDate()}/${
      fechaI.getMonth() + 1
    }/${fechaI.getFullYear()}`;
    this.fechaFin = `${fechaF.getDate()}/${
      fechaF.getMonth() + 1
    }/${fechaF.getFullYear()}`;

    this.campanniaService
      .getCampannas(fechasSeleccionadas.fechaIn, fechasSeleccionadas.fechaFin)
      .subscribe((boards) => {
        this.reiniciaVariables();

        this.camXCia = boards.tabla;

        this.campannas = boards.campannas;

        //asignando valores a pestaña venta nueva
        this.outbound[0] = {
          leads: boards.boardVentaNueva.outbound.leads,
          ventas: boards.boardVentaNueva.outbound.npc,
          conversion: this.trunc(boards.boardVentaNueva.outbound.conversion),
        };

        this.interaccionesOnline[0] = {
          leads: boards.boardVentaNueva.interaccionOnline.leads,
          ventas: boards.boardVentaNueva.interaccionOnline.npc,
          conversion: this.trunc(
            boards.boardVentaNueva.interaccionOnline.conversion
          ),
        };

        this.interaccionesTelefonica[0] = {
          leads: boards.boardVentaNueva.interaccionTelefonica.leads,
          ventas: boards.boardVentaNueva.interaccionTelefonica.npc,
          conversion: this.trunc(
            boards.boardVentaNueva.interaccionTelefonica.conversion
          ),
        };

        this.inbound[0] = {
          leads: boards.boardVentaNueva.inbound.leads,
          ventas: boards.boardVentaNueva.inbound.npc,
          conversion: this.trunc(boards.boardVentaNueva.inbound.conversion),
        };

        this.otros[0] = {
          leads: boards.boardVentaNueva.otros.leads,
          ventas: boards.boardVentaNueva.otros.npc,
          conversion: this.trunc(boards.boardVentaNueva.otros.conversion),
        };

        this.mejorPrecio[0] = {
          leads: boards.boardVentaNueva.mejorPrecio.leads,
          ventas: boards.boardVentaNueva.mejorPrecio.npc,
          conversion: this.trunc(boards.boardVentaNueva.mejorPrecio.conversion),
        };

        this.cobranza[0] = {
          leads: boards.boardVentaNueva.cobranza.leads,
          ventas: boards.boardVentaNueva.cobranza.npc,
          conversion: this.trunc(boards.boardVentaNueva.cobranza.conversion),
        };

        this.retencion[0] = {
          leads: boards.boardVentaNueva.retencion.leads,
          ventas: boards.boardVentaNueva.retencion.npc,
          conversion: this.trunc(boards.boardVentaNueva.retencion.conversion),
        };

        /////////asignando valores a pestaña Gastos Médicos
        this.outbound[1] = {
          leads: boards.boardGMM.outbound.leads,
          ventas: boards.boardGMM.outbound.npc,
          conversion: this.trunc(boards.boardGMM.outbound.conversion),
        };

        this.inbound[1] = {
          leads: boards.boardGMM.inbound.leads,
          ventas: boards.boardGMM.inbound.npc,
          conversion: this.trunc(boards.boardGMM.inbound.conversion),
        };
        this.mejorPrecio[1] = {
          leads: boards.boardGMM.mejorPrecio.leads,
          ventas: boards.boardGMM.mejorPrecio.npc,
          conversion: this.trunc(boards.boardGMM.mejorPrecio.conversion),
        };

        this.cobranza[1] = {
          leads: boards.boardGMM.cobranza.leads,
          ventas: boards.boardGMM.cobranza.npc,
          conversion: this.trunc(boards.boardGMM.cobranza.conversion),
        };

        this.retencion[1] = {
          leads: boards.boardGMM.retencion.leads,
          ventas: boards.boardGMM.retencion.npc,
          conversion: this.trunc(boards.boardGMM.retencion.conversion),
        };

        ////////asignando valores a pestaña Peru
        this.outbound[2] = {
          leads: boards.boardPeru.outbound.leads,
          ventas: boards.boardPeru.outbound.npc,
          conversion: this.trunc(boards.boardPeru.outbound.conversion),
        };

        this.inbound[2] = {
          leads: boards.boardPeru.inbound.leads,
          ventas: boards.boardPeru.inbound.npc,
          conversion: this.trunc(boards.boardPeru.inbound.conversion),
        };
        this.mejorPrecio[2] = {
          leads: boards.boardPeru.mejorPrecio.leads,
          ventas: boards.boardPeru.mejorPrecio.npc,
          conversion: this.trunc(boards.boardPeru.mejorPrecio.conversion),
        };

        this.cobranza[2] = {
          leads: boards.boardPeru.cobranza.leads,
          ventas: boards.boardPeru.cobranza.npc,
          conversion: this.trunc(boards.boardPeru.cobranza.conversion),
        };

        this.retencion[2] = {
          leads: boards.boardPeru.retencion.leads,
          ventas: boards.boardPeru.retencion.npc,
          conversion: this.trunc(boards.boardPeru.retencion.conversion),
        };

        this.subTotal = boards.subtotal;
        this.totalGeneral[0] = boards.totalGeneral;
        this.totalGeneral[1] = boards.totalGeneral;
        this.totalGeneral[2] = boards.totalGeneral;
        this.loader = false;
      });
  }

  reiniciaVariables() {
    this.vacio = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.outbound = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.interaccionesOnline = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.interaccionesTelefonica = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.inbound = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.otros = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.gastosMedicos = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.peru = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.fullOnline = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.mejorPrecio = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.cobranza = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.retencion = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.subTotal = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];
    this.totalGeneral = [
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
      {
        leads: 0,
        ventas: 0,
        conversion: 0,
      },
    ];

    this.campannas = [
      {
        tipoSubarea: '---',
        compannia: '---',
        tipoContacto: '---',
        contactosProd: 0,
        npc: 0,
        conversion: 0,
        pCobrada: 0,
      },
    ];

    this.camXCia = [
      {
        campanaGral: '---',
        outLead: 0,
        outNpc: 0,
        outConv: 0,
        telLead: 0,
        telNpc: 0,
        telConv: 0,
        inLead: 0,
        inNpc: 0,
        inConv: 0,
        onlLead: 0,
        onlNpc: 0,
        onlConv: 0,
        gralLead: 0,
        gralNpc: 0,
        gralConv: 0,
        pCobrada: 0,
      },
    ];
  }

  /**
   * Trunca los decimales de un número recibido
   * @param x numero que deseas convertir
   * @param posiciones de decimales a los que deseamos truncar
   * @returns resultado tipo number
   */
  trunc(x: number, posiciones = 3) {
    var s = x.toString();
    var l = s.length;
    var decimalLength = s.indexOf('.') + 1;
    var numStr = s.substr(0, decimalLength + posiciones);
    return Number(numStr);
  }

  leeDescargaHabilitada(habilitado: boolean) {
    this.descargaHabilitada = habilitado;
  }

  /**
   * Cada vez que se presione el botón 'Generar Reporte'
   * vamos a asignar el objeto recibido de la petición a la API,
   * el objeto es tabla, que ya contiene todos los datos
   * procesados y ordenados
   * @returns
   */
  descargaCamxCia() {
    if (!this.descargaHabilitada) {
      alert('Debes generar un reporte antes de descargar.');
      return;
    }
    const headers = [
      'campanaGral',
      'outLead',
      'outNpc',
      'outConv',
      'telLead',
      'telNpc',
      'telConv',
      'inLead',
      'inNpc',
      'inConv',
      'onlLead',
      'onlNpc',
      'onlConv',
      'gralLead',
      'gralNpc',
      'gralConv',
      'pCobrada',
    ];
    let titulo = `camxcia_${this.fechaInicio}_al_${this.fechaFin}`;

    this.xlsxService.imprimirXLS(this.camXCia, headers, titulo);
  }

  descargaCampannas() {
    if (!this.descargaHabilitada) {
      alert('Debes generar un reporte antes de descargar.');
      return;
    }
    const headers = [
      'tipoSubarea',
      'compannia',
      'tipoContacto',
      'contactosProd',
      'npc',
      'conversion',
      'pCobrada',
    ];
    let titulo = `campañas_${this.fechaInicio}_al_${this.fechaFin}`;

    this.xlsxService.imprimirXLS(this.campannas, headers, titulo);
  }
}
