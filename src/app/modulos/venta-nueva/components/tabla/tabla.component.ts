import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { VNDatos } from 'src/app/@core/models/Venta-Nueva/VNDatos';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css'],
})
export class TablaComponent implements OnInit, OnChanges {
  @Input() color: string;
  @Input() titulo: string;
  @Input() columnas: string[];
  @Input() encabezadoCol: string[];
  @Input() datos: VNDatos[];

  displayedColumns: string[];
  dataSource;

  formateador = new Intl.NumberFormat('en-US');
  datosFormateados: VNDatos[];

  resaltarSubtotal: boolean;
  resaltarTotalGral: boolean;

  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.formatearDatos();
    this.dataSource = this.datos;
  }

  ngOnInit(): void {
    this.displayedColumns = this.columnas;
    /**
     * Si la tabla se llama Subtotal o Total General,
     * las vamos a resaltar con una clase
     */
    this.titulo === 'Subtotal'
      ? (this.resaltarSubtotal = true)
      : (this.resaltarSubtotal = false);
    this.titulo === 'Total General'
      ? (this.resaltarTotalGral = true)
      : (this.resaltarTotalGral = false);
    this.formatearDatos();
    this.dataSource = this.datos;
  }

  formatearDatos() {
    this.datos.map((entrada) => {
      if (typeof entrada.leads === 'string') {
        return;
      }
      entrada.leads = this.formateador.format(entrada.leads as number);
      entrada.ventas = this.formateador.format(entrada.ventas as number);
    });
  }
}
