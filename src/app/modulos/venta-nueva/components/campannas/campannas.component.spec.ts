import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampannasComponent } from './campannas.component';

describe('CampannasComponent', () => {
  let component: CampannasComponent;
  let fixture: ComponentFixture<CampannasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampannasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampannasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
