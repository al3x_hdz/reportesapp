import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Campanna } from 'src/app/@core/models/Venta-Nueva/Campanna';

@Component({
  selector: 'app-campannas',
  templateUrl: './campannas.component.html',
  styleUrls: ['./campannas.component.css'],
})
export class CampannasComponent implements OnInit, OnChanges {
  @Input() tabla: Campanna[];
  formateador = new Intl.NumberFormat('en-US');

  dataSource;
  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = this.tabla;
  }

  ngOnInit(): void {
    this.dataSource = this.tabla;
  }

  displayedColumns: string[] = [
    'tipoSubarea',
    'compannia',
    'tipoContacto',
    'contactosProd',
    'npc',
    'conversion',
    'pCobrada',
  ];
}
