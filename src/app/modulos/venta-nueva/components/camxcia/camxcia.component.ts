import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { camXCiaRow } from 'src/app/@core/models/Venta-Nueva/camXCiaRow';

@Component({
  selector: 'app-camxcia',
  templateUrl: './camxcia.component.html',
  styleUrls: ['./camxcia.component.css'],
})
export class CamxciaComponent implements OnInit, OnChanges {
  @Input() tabla: camXCiaRow[];
  formateador = new Intl.NumberFormat('en-US');
  dataSource;

  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = this.tabla;
  }

  ngOnInit(): void {
    this.dataSource = this.tabla;
  }

  displayedColumns: string[] = [
    'campanaGral',
    'outLead',
    'outNpc',
    'outConv',
    'telLead',
    'telNpc',
    'telConv',
    'inLead',
    'inNpc',
    'inConv',
    'onlLead',
    'onlNpc',
    'onlConv',
    'gralLead',
    'gralNpc',
    'gralConv',
    'pCobrada',
  ];
}
