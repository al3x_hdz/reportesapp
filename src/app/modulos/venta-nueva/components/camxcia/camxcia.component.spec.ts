import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamxciaComponent } from './camxcia.component';

describe('CamxciaComponent', () => {
  let component: CamxciaComponent;
  let fixture: ComponentFixture<CamxciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamxciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CamxciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
