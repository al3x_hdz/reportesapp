import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modulos',
  template: `
    <app-toolbar></app-toolbar>
    <router-outlet></router-outlet>
  `,
})
export class ModulosComponent {}
