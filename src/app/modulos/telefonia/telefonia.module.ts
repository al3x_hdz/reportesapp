import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TelefoniaRoutingModule } from './telefonia-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, TelefoniaRoutingModule],
})
export class TelefoniaModule {}
