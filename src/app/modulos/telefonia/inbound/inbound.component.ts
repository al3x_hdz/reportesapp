import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { LlamadasEntrantes } from '../../../@core/models/LlamadasEntrantes';
import { LlamadasEntrantesService } from 'src/app/@core/services/Telefonia/llamadas-entrantes.service';

@Component({
  selector: 'app-inbound',
  templateUrl: './inbound.component.html',
  styleUrls: ['./inbound.component.css'],
})
export class InboundComponent implements OnInit {
  @Output() loader = new EventEmitter<boolean>();

  tituloAtendidas = 'Llamadas Atendidas y No Atendidas Por El Ejecutivo';
  tituloGraficoMes = 'Total de Llamadas y Estatus';

  todasLasLlamadas: LlamadasEntrantes[];
  todasLasLlamadasFiltros: LlamadasEntrantes[];

  fechaActual: Date = new Date();

  columnasAtendidas = ['Ejecutivos', 'No Ejecutivos'];
  estatus = [
    'Abandonada',
    'Contestada',
    'Contestada No Finalizada',
    'Fuera de Horario',
    'IVR',
    'Transferida a ATC',
    'Transferida a Pagos',
    'Transferida a Renovaciones',
    'Transferida a Siniestros',
  ];

  colores = [
    '#008FFB',
    '#00E396',
    '#FEB019',
    '#FF4560',
    '#775DD0',
    '#00D9E9',
    '#FF66C3',
    '#C67219',
    '#838383',
  ];

  meses = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];
  mesActual = this.meses[this.fechaActual.getMonth()];

  filtrosArgumentos = [];
  filtrosSeleccionados = [
    { columna: 'fechaInicio', valor: this.mesActual },
    { columna: 'opcionReal', valor: '' },
    { columna: 'areaIVR', valor: '' },
    { columna: 'campanaIVR', valor: '' },
    { columna: 'queue', valor: '' },
    { columna: 'area', valor: '' },
    { columna: 'ClasificaciónLlamadas', valor: '' },
  ];

  constructor(private llamadasEntrantesService: LlamadasEntrantesService) {}

  ngOnInit(): void {
    try {
      this.traeReporteLlamadasEntrantes();
    } catch (error) {
      console.error(error);
    } finally {
      this.generaFiltrosArgumentos();
    }
  }

  /**
   * Funcion que invoca al servicio que que nos trae los reportes completos,
   * y nos divide la informacion en las variables respectivas
   * del componente
   */
  traeReporteLlamadasEntrantes(): void {
    this.loader.emit(true);
    this.llamadasEntrantesService
      .getLlamadasEntrantesMes(
        this.fechaActual.getMonth(),
        this.fechaActual.getFullYear()
      )
      .subscribe({
        next: (data) => {
          this.todasLasLlamadas = data;
        },
        error: (err) => {},
        complete: () => {
          this.todasLasLlamadasFiltros = this.todasLasLlamadas;

          this.loader.emit(false);
        },
      });
  }

  generaFiltrosArgumentos(): void {
    this.filtrosArgumentos = [
      {
        opciones: this.meses,
        titulo: 'Mensual',
        placeholder: 'Selecciona un mes',
        defaultValue: this.fechaActual.getMonth(),
      },
      {
        opciones: ['0', '1', '2', '3', '4', 'i', 't'],
        titulo: 'Opción',
        placeholder: 'Seleccione una opción',
        defaultValue: '',
      },
      {
        opciones: [
          'Cajas Stronger',
          'Gastos Medicos',
          'LlamadaSinIVR',
          'Renovaciones',
          'Venta Nueva',
          'Venta Nueva Perú',
        ],
        titulo: 'Área IVR',
        placeholder: 'Seleccione un área',
        defaultValue: '',
      },
      {
        opciones: [
          'ABA',
          'AFIRME',
          'AHORRA',
          'AIG',
          'ANA',
          'ATLAS',
          'AXA',
          'BANORTE',
          'CAJASST',
          'CAJASSTRONGER',
          'CHOFERPRIVADO',
          'COMPARAYA',
          'ELAGUILA',
          'ELPOTOSI',
          'GASTOSGNP',
          'GENERALDESEGUROS',
          'GNPBRAND',
          'HDI',
          'INBURSA',
          'LALATINO',
          'MAPFRE',
          'MAPFREPERU',
          'MIGO',
          'MOTOS',
          'MULTIMARCAS',
          'MULTIMARCASDOS',
          'QUALITAS',
          'QUALITASPERU',
          'RENOABA',
          'RENOAFIRME',
          'RENOAGUILA',
          'RENOATLAS',
          'RENOAXA',
          'RENOBANORTE',
          'RENODIGANA',
          'RENODIGAXA',
          'RENODIGGNP',
          'RENODIGMULTI',
          'RENODIGQUAL',
          'RENOGNP',
          'RENOHDI',
          'RENOINBUR',
          'RENOMAPFRE',
          'RENOMOTOS',
          'RENOMULTIVA',
          'RENOQUALITAS',
          'RENOTAXIS',
          'RENOUBER',
          'RENOZURICH',
          'RIMACPERU',
          'TAXIS',
          'TIAHORRADOS',
          'ZURICH',
        ],
        titulo: 'Campaña IVR',
        placeholder: 'Seleccione campaña',
        defaultValue: '',
      },
      {
        opciones: [
          'ABAECOMM',
          'ABAIN',
          'ABAINTE',
          'AHORRAIN',
          'AHORRAINTE',
          'BANORTEIN',
          'GASTOSGNPIN',
          'GASTOSMAPFREIN',
          'GNPINTE',
          'HDIECOMM',
          'MAPFREPERU',
          'MULTIMARCASECOMM',
          'MULTIMARCASGMM',
          'QUALIECOMM',
          'QUALITASINTE',
          'QUALITASPERU',
          'RENOABAIN',
          'RENOAFIRMEIN',
          'RENOAGUILAIN',
          'RENOAXAIN',
          'RENOBANORTEIN',
          'RENODIGAXAIN',
          'RENODIGGNPIN',
          'RENODIGMULTIIN',
          'RENODIGQUALIN',
          'RENOGNPIN',
          'RENOHDIIN',
          'RENOINBURIN',
          'RENOMAPFREIN',
          'RENOMOTOSIN',
          'RENOMULTIMARCASDOSIN',
          'RENOMULTIMARCASIN',
          'RENOQUALITASIN',
          'RENORECUMULTI',
          'RENOTAXISIN',
          'RENOUBERIN',
          'RENOZURICHIN',
          'RIMACPERU',
          'TIAHORRADOS',
          'VNAFIRMEIN',
          'VNCOMPARADORESIN',
          'VNGNPBRANDIN',
          'VNMOTOSQUALITAS',
          'VNMULTIMARCASIN',
          'VNQUALITASBRANDIN',
          'VNTRACTOCAMIONES',
        ],
        titulo: 'Campaña Grupos',
        placeholder: 'Seleccione campaña',
        defaultValue: '',
      },
      {
        opciones: [
          'ATENCION A CLIENTES',
          'AUDITORIA INTERNA',
          'DESARROLLO',
          'FINANZAS',
          'GMM',
          'RN DIG CA INT',
          'RN DIGITAL CA EXT',
          'RN RECUPERACION',
          'RN TRADICIONAL',
          'SEGUIMIENTO',
          'TECNOLOGIAS DE LA INFORMACION',
          'VENTA NUEVA',
          'VN IN FS',
          'VN IN MT',
          'VN IN VP',
          'VN INT ONLINE',
          'VN INT TELEFONICA',
        ],
        titulo: 'Area',
        placeholder: 'Seleccione un área',
        defaultValue: '',
      },
      {
        opciones: ['Atendidas por Ejecutivos', 'No Atendidas por Ejecutivos'],
        titulo: 'Clasificación de Llamadas',
        placeholder: 'Seleccione una clasificación',
        defaultValue: '',
      },
    ];
  }

  /**
   * Filtra en cascada, basandonos en las tarjetas de la vista
   * si el filtro no tiene ningún valor se omnite
   * @param filtro arreglo de objetos que tienen la columna y valor, que vamos
   * a buscar en las llamadas
   */
  leeFiltroCard(filtro: any): void {
    this.filtrosSeleccionados[filtro.idFiltro].valor = filtro.seleccion;
    this.todasLasLlamadasFiltros = this.todasLasLlamadas;
    let aux;

    // if (this.filtrosSeleccionados[0].valor !== '') {
    //   aux = this.todasLasLlamadasFiltros.filter((llamada) => {
    //     let fecha = new Date(llamada.fechaInicio);
    //     let fechaGen = new Date(llamada.fechaGen);

    //     if (
    //       this.meses[fecha.getMonth()] === this.filtrosSeleccionados[0].valor ||
    //       this.meses[fechaGen.getMonth()] === this.filtrosSeleccionados[0].valor
    //     ) {
    //       return llamada;
    //     } else {
    //       return null;
    //     }

    //     // return this.meses[fecha.getMonth()] ===
    //     //   this.filtrosSeleccionados[0].valor
    //     //   ? llamada
    //     //   : null;
    //   });
    //   this.todasLasLlamadasFiltros = aux;
    // }

    if (this.filtrosSeleccionados[1].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[1].columna] ===
          this.filtrosSeleccionados[1].valor
      );
      this.todasLasLlamadasFiltros = aux;
    }

    if (this.filtrosSeleccionados[2].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[2].columna] ===
          this.filtrosSeleccionados[2].valor
      );
      this.todasLasLlamadasFiltros = aux;
    }

    if (this.filtrosSeleccionados[3].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[3].columna] ===
          this.filtrosSeleccionados[3].valor
      );

      this.todasLasLlamadasFiltros = aux;
    }

    if (this.filtrosSeleccionados[4].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[4].columna] ===
          this.filtrosSeleccionados[4].valor
      );

      this.todasLasLlamadasFiltros = aux;
    }

    if (this.filtrosSeleccionados[5].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[5].columna] ===
          this.filtrosSeleccionados[5].valor
      );

      this.todasLasLlamadasFiltros = aux;
    }

    if (this.filtrosSeleccionados[6].valor !== '') {
      aux = this.todasLasLlamadasFiltros.filter(
        (llamada) =>
          llamada[this.filtrosSeleccionados[6].columna] ===
          this.filtrosSeleccionados[6].valor
      );

      this.todasLasLlamadasFiltros = aux;
    }
  }
}
