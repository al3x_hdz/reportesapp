import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../@theme/shared/shared.module';
import { NgApexchartsModule } from 'ng-apexcharts';

import { InboundComponent } from './inbound.component';
import { StackedChartComponent } from './components/charts/stacked-chart/stacked-chart.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { CardComponent } from './components/card/card.component';
import { MixChartsComponent } from './components/charts/mix-charts/mix-charts.component';
import { FiltroCardComponent } from './components/filtro-card/filtro-card.component';
import { SepararPorHoraPipe } from 'src/app/@core/pipes/telefonia/separar-por-hora.pipe';
import { ExtraerLlamadasDelMesPipe } from 'src/app/@core/pipes/telefonia/extraer-llamadas-del-mes.pipe';
import { SepararLlamadasPorDiaPipe } from 'src/app/@core/pipes/telefonia/separar-llamadas-por-dia.pipe';

@NgModule({
  declarations: [
    InboundComponent,
    StackedChartComponent,
    BarChartComponent,
    CardComponent,
    MixChartsComponent,
    FiltroCardComponent,
    SepararPorHoraPipe,
    ExtraerLlamadasDelMesPipe,
    SepararLlamadasPorDiaPipe,
  ],
  imports: [CommonModule, SharedModule, NgApexchartsModule],
  exports: [InboundComponent],
})
export class InboundModule {}
