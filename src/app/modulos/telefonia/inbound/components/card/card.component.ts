import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { LlamadasEntrantes } from 'src/app/@core/models/LlamadasEntrantes';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit, OnChanges {
  @Input() datos: LlamadasEntrantes[];

  formateador = new Intl.NumberFormat('en-US');
  totalLlamadasFormateado: string;
  numerosUnicoFormateado: string;

  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.totalLlamadasFormateado = this.formateador.format(this.datos.length);
    this.numerosUnicoFormateado = this.formateador.format(this.numerosUnicos());
  }

  /**
   * De entre las llamadas entrantes, cuenta los numeros telefónicos sin repetir
   * @returns resultasdos tipo number
   */
  numerosUnicos(): number {
    let numeros = this.datos.map((llamada) => llamada.telefonoCliente);

    let result = numeros.filter((item, index) => {
      return numeros.indexOf(item) === index;
    });

    return result.length;
  }
}
