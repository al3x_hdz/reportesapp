import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-filtro-card',
  templateUrl: './filtro-card.component.html',
  styleUrls: ['./filtro-card.component.css'],
})
export class FiltroCardComponent implements OnInit {
  @Input() id: number;
  @Input() titulo: string;
  @Input() opciones: string[];
  @Input() placeholder: string;
  @Input() defaultValue: number;

  @Output() opcionSeleccionada = new EventEmitter<any>();

  options: any[] = [];
  seleccionado: string;

  opcionesDescripcion = [
    '1 (Reporte de siniestro)',
    '2 (Adquisicón de póliza)',
    '3 (Pago de póliza)',
    '4 (Renovación de póliza)',
    '5 (ATC)',
    'i (Opción inválida)',
    't (Tiempo espera terminado)',
  ];

  constructor() {}

  ngOnInit(): void {
    for (const key in this.opciones) {
      this.options.push({
        value: parseInt(key),
        viewValue: this.opciones[key],
      });
    }
    /**
     * Para el filtro "opción" debemos darle trato especial
     * para que las opciones depleglables sean descriptivas
     */
    if (this.id === 1) {
      for (const key in this.opciones) {
        this.options[key].value = this.options[key].viewValue;
        this.options[key].viewValue = this.opcionesDescripcion[key];
      }
    }
  }

  nuevaSeleccion(event: MatSelectChange) {
    let filtro;
    try {
      if (this.id === 1) {
        filtro = {
          idFiltro: this.id,
          seleccion: this.options[event.value].value,
        };
      } else {
        filtro = {
          idFiltro: this.id,
          seleccion: this.options[event.value].viewValue,
        };
      }
    } catch (error) {
      filtro = {
        idFiltro: this.id,
        seleccion: '',
      };
    }
    this.opcionSeleccionada.emit(filtro);
  }
}
