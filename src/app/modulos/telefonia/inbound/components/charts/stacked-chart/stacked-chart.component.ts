import {
  Component,
  OnInit,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { LlamadasEstatusService } from 'src/app/@core/services/Telefonia/llamadas-estatus.service';

//Apexcharts imports
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexXAxis,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexYAxis,
  ApexTooltip,
  ApexFill,
  ApexLegend,
} from 'ng-apexcharts';

//Apexcharts exports
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  colors: string[];
  tooltip: ApexTooltip;
  fill: ApexFill;
  legend: ApexLegend;
};

@Component({
  selector: 'app-stacked-chart',
  templateUrl: './stacked-chart.component.html',
  styleUrls: ['./stacked-chart.component.css'],
})
export class StackedChartComponent implements OnInit, OnChanges {
  @Input() datos: any; //Reibe el arreglo con las llamadas del mes
  @Input() yTitulos: string[]; //recibe un arreglo con el titulo de las filas
  @Input() tituloGrafico: string; //recibe el titulo de la grafica
  @Input() estatus: string[];
  @Input() colores: string[];

  private totalLlamadas: number;

  /**
   * Va a almacenar las llamadas recibidas en el input datos
   * pero separadas por ClasificaciónLlamadas
   */
  private llamadasEjecutivo = {
    ejecutivo: [],
    noEjecutivo: [],
  };

  private datosGrafico = []; //objeto que se pasará a series para el grafico

  @ViewChild('chart') chart: ChartComponent;
  //Almacena la grafica y sus configuraciones
  public chartOptions: Partial<ChartOptions>;

  constructor(private llamadasEstatusService: LlamadasEstatusService) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.inicializaDatosGrafico();
    this.yTitulos.length > 1
      ? this.procesaDatosEjecutivos()
      : this.procesaDatosMes();

    this.totalLlamadas = this.datos.length;
    this.generaGrafico();
  }

  /**
   * Inicializamos la variable datosGrafico para
   * solo asignarle valor a data
   */
  inicializaDatosGrafico() {
    this.datosGrafico = [
      {
        name: this.estatus[0],
        data: [],
      },
      {
        name: this.estatus[1],
        data: [],
      },
      {
        name: this.estatus[2],
        data: [],
      },
      {
        name: this.estatus[3],
        data: [],
      },
      {
        name: this.estatus[4],
        data: [],
      },
      {
        name: this.estatus[5],
        data: [],
      },
      {
        name: this.estatus[6],
        data: [],
      },
      {
        name: this.estatus[7],
        data: [],
      },
      {
        name: this.estatus[8],
        data: [],
      },
    ];
  }

  /**
   * Divide las llamadas del mes entre los diferentes estatus
   * que existen para una llamada entrante
   */
  procesaDatosMes() {
    this.datosGrafico[0].data.push(
      this.llamadasEstatusService.llamadasAbandonadas(this.datos).length
    );
    this.datosGrafico[1].data.push(
      this.llamadasEstatusService.llamadasContestadas(this.datos).length
    );
    this.datosGrafico[2].data.push(
      this.llamadasEstatusService.llamadasContestadasNoFinalizadas(this.datos)
        .length
    );
    this.datosGrafico[3].data.push(
      this.llamadasEstatusService.llamadasFueraHorario(this.datos).length
    );
    this.datosGrafico[4].data.push(
      this.llamadasEstatusService.ivr(this.datos).length
    );
    this.datosGrafico[5].data.push(
      this.llamadasEstatusService.tranferidaATC(this.datos).length
    );
    this.datosGrafico[6].data.push(
      this.llamadasEstatusService.tranferidaPagos(this.datos).length
    );
    this.datosGrafico[7].data.push(
      this.llamadasEstatusService.tranferidaRenovaciones(this.datos).length
    );
    this.datosGrafico[8].data.push(
      this.llamadasEstatusService.tranferidaSiniestros(this.datos).length
    );
  }

  /**
   * Primero va llamar a la funcion divideLlamadas()
   * si en la iteracion del ciclo for key es ejecutivo,
   * va a enviar como parametro 'Atendidas por Ejecutivos, de lo
   * contrario manda 'No Atendidas por Ejecutivos'
   *
   * una vez definidos los valores del objeto llamadasEjecutivo,
   * el siguiente ciclo for va a asignar los valores de data
   * en la variable datosGrafico
   */
  procesaDatosEjecutivos() {
    for (let key in this.llamadasEjecutivo) {
      this.llamadasEjecutivo[key] = this.divideLlamadas(
        key === 'ejecutivo'
          ? 'Atendidas por Ejecutivos'
          : 'No Atendidas por Ejecutivos'
      );
    }

    for (const key in this.llamadasEjecutivo) {
      this.datosGrafico[0].data.push(
        this.llamadasEstatusService.llamadasAbandonadas(
          this.llamadasEjecutivo[key]
        ).length
      );
      this.datosGrafico[1].data.push(
        this.llamadasEstatusService.llamadasContestadas(
          this.llamadasEjecutivo[key]
        ).length
      );
      this.datosGrafico[2].data.push(
        this.llamadasEstatusService.llamadasContestadasNoFinalizadas(
          this.llamadasEjecutivo[key]
        ).length
      );
      this.datosGrafico[3].data.push(
        this.llamadasEstatusService.llamadasFueraHorario(
          this.llamadasEjecutivo[key]
        ).length
      );
      this.datosGrafico[4].data.push(
        this.llamadasEstatusService.ivr(this.llamadasEjecutivo[key]).length
      );
      this.datosGrafico[5].data.push(
        this.llamadasEstatusService.tranferidaATC(this.llamadasEjecutivo[key])
          .length
      );
      this.datosGrafico[6].data.push(
        this.llamadasEstatusService.tranferidaPagos(this.llamadasEjecutivo[key])
          .length
      );
      this.datosGrafico[7].data.push(
        this.llamadasEstatusService.tranferidaRenovaciones(
          this.llamadasEjecutivo[key]
        ).length
      );
      this.datosGrafico[8].data.push(
        this.llamadasEstatusService.tranferidaSiniestros(
          this.llamadasEjecutivo[key]
        ).length
      );
    }
  }

  /**
   *  Procesa todas las entradas del input datos y si
   * coincide el atributo ClasificacionLlamadas con el filtro
   * se añade a un arreglo auxiliar que será el resultado
   * @param filtro puede ser 'Atendidas por Ejecutivos' || 'No Atendidas por Ejecutivos'
   * @returns arreglo con los resultados del filtro
   */
  divideLlamadas(filtro: string) {
    let aux = [];
    for (const iterator of this.datos) {
      if (iterator.ClasificaciónLlamadas === filtro) {
        aux.push(iterator);
      }
    }
    return aux;
  }

  /**
   * Genera el gráfico dentro de la variable chartOptions,
   * lee las llamadas filtradas dentro de la variable datosGrafico
   * tiene un configuracion para las barras horizontales,
   * titulso en eje y de la variable yTitulos
   *
   */
  generaGrafico(): void {
    let totalLlamadas = 0;

    for (const index in this.datos) {
      totalLlamadas += this.datos[index].data;
    }

    this.chartOptions = {
      series: this.datosGrafico,
      chart: {
        type: 'bar',
        height: 250,
        stacked: true,
        toolbar: {
          show: false,
        },
        animations: {
          speed: 1500,
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: number) {
          if (val !== 0) {
            return `${Intl.NumberFormat('en-US').format(val)}`;
          } else {
            return '';
          }
        },
        distributed: true,
      },
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      stroke: {
        width: 1,
        colors: ['#fff'],
      },
      title: {
        text: this.tituloGrafico,
        align: 'center',
      },
      colors: this.colores,
      xaxis: {
        categories: this.yTitulos,
        title: {
          text: `Total de Llamadas: ${Intl.NumberFormat('en-US').format(
            this.totalLlamadas
          )}`,
        },
      },
      yaxis: {
        title: {
          text: 'Mes',
        },
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return `${Intl.NumberFormat('en-US').format(val)}`;
          },
        },
      },
      fill: {
        opacity: 1,
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 40,
      },
    };
  }
}
