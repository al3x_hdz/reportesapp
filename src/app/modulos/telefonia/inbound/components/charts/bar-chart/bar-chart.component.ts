import {
  Component,
  OnInit,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { LlamadasEstatusService } from 'src/app/@core/services/Telefonia/llamadas-estatus.service';

import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexTitleSubtitle,
  ApexYAxis,
  ApexLegend,
  ApexStroke,
  ApexXAxis,
  ApexFill,
  ApexTooltip,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  title: ApexTitleSubtitle;
  colors: string[];
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
};

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css'],
})
export class BarChartComponent implements OnInit, OnChanges {
  /*datos: objeto de arreglos que contiene las llamadasActuales, llamadasHoraAnterior y llamdasDosHorasAnteriores*/
  @Input() datos: any;
  @Input() fechaYHora: Date;
  @Input() estatus: string[];
  @Input() colores: string[];

  //almacena el arreglo que definira series del objeto chartOptions
  private datosGrafico: any[] = [];
  //alamcena el titulo que aparecerá en el grafico
  private titulo: string;
  //Almacena los titulos que apareceran en el eje x
  private categorias: string[];

  //llamadas entrantes por estatus
  private filtroHoras;

  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private llamadasEstatusService: LlamadasEstatusService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.procesaDatos();
    this.generaGrafico();
  }

  ngOnInit(): void {
    this.generaTitulo();
    this.generaCategorias();
    this.procesaDatos();
    this.generaGrafico();
  }

  /**
   * separamos los datos por hora, se descompone por estatus, se cuentan y se
   * almacenan en filtroHoras
   * Ejemplo
   * filtroHoras = {
   *   abandonadas: [14,22,33,44,55],
   *   contestadas:[11,22,33,44,55]
   * }
   **/
  procesaDatos(): void {
    //reinicio de variables para actualizar grafico
    this.filtroHoras = {
      abandonadas: [],
      contestadas: [],
      contestadasNoFinalizadas: [],
      fueraDeHorario: [],
      ivr: [],
      transferidaAtc: [],
      transferidaPagos: [],
      transferidaRenovaciones: [],
      transferidaSiniestros: [],
    };

    this.datosGrafico = [];

    for (const key in this.datos) {
      this.filtroHoras.abandonadas.push(
        this.llamadasEstatusService.llamadasAbandonadas(this.datos[key]).length
      );
      this.filtroHoras.contestadas.push(
        this.llamadasEstatusService.llamadasContestadas(this.datos[key]).length
      );
      this.filtroHoras.contestadasNoFinalizadas.push(
        this.llamadasEstatusService.llamadasContestadasNoFinalizadas(
          this.datos[key]
        ).length
      );
      this.filtroHoras.fueraDeHorario.push(
        this.llamadasEstatusService.llamadasFueraHorario(this.datos[key]).length
      );
      this.filtroHoras.ivr.push(
        this.llamadasEstatusService.ivr(this.datos[key]).length
      );
      this.filtroHoras.transferidaAtc.push(
        this.llamadasEstatusService.tranferidaATC(this.datos[key]).length
      );
      this.filtroHoras.transferidaPagos.push(
        this.llamadasEstatusService.tranferidaPagos(this.datos[key]).length
      );
      this.filtroHoras.transferidaRenovaciones.push(
        this.llamadasEstatusService.tranferidaRenovaciones(this.datos[key])
          .length
      );
      this.filtroHoras.transferidaSiniestros.push(
        this.llamadasEstatusService.tranferidaSiniestros(this.datos[key]).length
      );
    }

    let totales = 0;
    let i = 0;
    /**
     * Verificamos si la suma de cada elemento de arreglo de filtroHoras
     * es diferente de 0, si se umple añadimos la barra al gráfico.
     * Esa suma se almacena en totales y se reinicia cada vez que termina el ciclo
     */
    for (const key in this.filtroHoras) {
      this.filtroHoras[key].map((element) => {
        totales += element;
      });
      totales !== 0
        ? this.datosGrafico.push({
            name: this.estatus[i],
            data: this.filtroHoras[key],
          })
        : null;
      totales = 0;
      i++;
    }
  }

  /**
   * Define el título a partir de la fecha actual
   */
  generaTitulo(): void {
    this.titulo =
      'Total de Llamadas y Estatus:' +
      ` ${this.fechaYHora.getDate()}/${
        this.fechaYHora.getMonth() + 1
      }/${this.fechaYHora.getFullYear()}`;
  }

  /**
   * Lee la variable fechaYHora para asignar el nombre a los
   * apartados que se presentan debajo del eje X
   */
  generaCategorias(): void {
    this.categorias = [];
    for (let key in this.datos) {
      parseInt(key) === this.fechaYHora.getHours()
        ? this.categorias.push(
            `${parseInt(key)}:00 a ${parseInt(key) + 1}:00 (Actual)`
          )
        : this.categorias.push(`${parseInt(key)}:00 a ${parseInt(key) + 1}:00`);
    }
  }

  generaGrafico(): void {
    this.chartOptions = {
      series: this.datosGrafico,
      chart: {
        type: 'bar',
        height: 350,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          dataLabels: {
            position: 'top',
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: number) {
          if (val !== 0) {
            return `${Intl.NumberFormat('en-US').format(val)}`;
          } else {
            return '';
          }
        },
        offsetY: -20,
        style: {
          colors: ['#304758'],
        },
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent'],
      },
      xaxis: {
        categories: this.categorias,
        title: {
          text: 'Hora',
        },
      },
      yaxis: {
        title: {
          text: 'Total de Llamadas',
        },
      },
      fill: {
        opacity: 1,
      },
      title: {
        text: this.titulo,
        align: 'center',
      },
      colors: this.colores,
      tooltip: {
        y: {
          formatter: function (val) {
            return Intl.NumberFormat('en-US').format(val);
          },
        },
      },
      legend: {
        position: 'right',
        showForNullSeries: false,
      },
    };
  }
}
