import {
  Component,
  OnInit,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { LlamadasEstatusService } from 'src/app/@core/services/Telefonia/llamadas-estatus.service';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexTooltip,
  ApexXAxis,
  ApexLegend,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexYAxis,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  markers: any; //ApexMarkers;
  stroke: any; //ApexStroke;
  yaxis: ApexYAxis | ApexYAxis[];
  dataLabels: ApexDataLabels;
  title: ApexTitleSubtitle;
  colors: string[];
  legend: ApexLegend;
  fill: ApexFill;
  tooltip: ApexTooltip;
};

@Component({
  selector: 'app-mix-charts',
  templateUrl: './mix-charts.component.html',
  styleUrls: ['./mix-charts.component.css'],
})
export class MixChartsComponent implements OnInit, OnChanges {
  @Input() datos: any; // recibe las llamadas entrantes
  @Input() fechaYHora: Date;
  @Input() estatus: string[];
  @Input() colores: string[];

  colors: string[];
  xTitulos: string[] = [];
  private datosGrafico: any[];

  //inicializacion del objeto que se asigna a atributo series del objeto chartOptions
  private filtroDias = {
    abandonadas: [],
    contestadas: [],
    contestadasNoFinalizadas: [],
    fueraDeHorario: [],
    ivr: [],
    transferidaAtc: [],
    transferidaPagos: [],
    transferidaRenovaciones: [],
    transferidaSiniestros: [],
  };

  //guarda el total de las llamadas realizadas cada día
  totalLlamadasPorDia: number[] = [];

  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private llamadasEstatuservice: LlamadasEstatusService) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.colors = this.colores;
    this.colors.push('#43CE00');

    this.procesaDatos(this.datos);
    this.generadDatosGrafico();
    this.generaXTitulos();
    this.generaGrafico();
  }

  /**
   * Genera los titulos que se mostraran en el eje x
   * en este caso son las fechas de cada día del mes
   */
  generaXTitulos(): void {
    for (const iterator in this.datos) {
      this.xTitulos.push(
        `${parseInt(iterator) + 1}/${
          this.fechaYHora.getMonth() + 1
        }/${this.fechaYHora.getFullYear()}`
      );
    }
  }

  /**
   * define el contenido de la variable
   * datosGrafico
   */
  generadDatosGrafico(): void {
    this.datosGrafico = [];

    this.datosGrafico = [
      {
        name: this.estatus[0],
        type: 'column',
        data: this.filtroDias.abandonadas,
      },
      {
        name: this.estatus[1],
        type: 'column',
        data: this.filtroDias.contestadas,
      },
      {
        name: this.estatus[2],
        type: 'column',
        data: this.filtroDias.contestadasNoFinalizadas,
      },
      {
        name: this.estatus[3],
        type: 'column',
        data: this.filtroDias.fueraDeHorario,
      },
      {
        name: this.estatus[4],
        type: 'column',
        data: this.filtroDias.ivr,
      },
      {
        name: this.estatus[5],
        type: 'column',
        data: this.filtroDias.transferidaAtc,
      },
      {
        name: this.estatus[6],
        type: 'column',
        data: this.filtroDias.transferidaPagos,
      },
      {
        name: this.estatus[7],
        type: 'column',
        data: this.filtroDias.transferidaRenovaciones,
      },
      {
        name: this.estatus[8],
        type: 'column',
        data: this.filtroDias.transferidaSiniestros,
      },
      {
        name: 'Total de Llamadas',
        type: 'line',
        data: this.totalLlamadasPorDia,
      },
    ];
  }

  /**
   * La funcion va a filtrar las llamadas que vienen en un
   * arreglo donde fueron divididos por días,
   * se separa las llamadas de cada dia en 4 categorias:
   * -Abandonadas
   * -Contestadas
   * -Contestadas no Finalizadas
   * -Fuera de Horario
   *
   * adicionalmente se va a llevar el conteo total
   * de todas las llamadas realizadas en un día
   *
   * @param datos arreglo input del componente
   */
  procesaDatos(datos) {
    //reinicio de variables para actualizar grafico
    this.filtroDias = {
      abandonadas: [],
      contestadas: [],
      contestadasNoFinalizadas: [],
      fueraDeHorario: [],
      ivr: [],
      transferidaAtc: [],
      transferidaPagos: [],
      transferidaRenovaciones: [],
      transferidaSiniestros: [],
    };

    this.totalLlamadasPorDia = [];

    for (const key in datos) {
      this.filtroDias.abandonadas.push(
        this.llamadasEstatuservice.llamadasAbandonadas(datos[key]).length
      );
      this.filtroDias.contestadas.push(
        this.llamadasEstatuservice.llamadasContestadas(datos[key]).length
      );
      this.filtroDias.contestadasNoFinalizadas.push(
        this.llamadasEstatuservice.llamadasContestadasNoFinalizadas(datos[key])
          .length
      );
      this.filtroDias.fueraDeHorario.push(
        this.llamadasEstatuservice.llamadasFueraHorario(datos[key]).length
      );
      this.filtroDias.ivr.push(
        this.llamadasEstatuservice.ivr(datos[key]).length
      );
      this.filtroDias.transferidaAtc.push(
        this.llamadasEstatuservice.tranferidaATC(datos[key]).length
      );
      this.filtroDias.transferidaPagos.push(
        this.llamadasEstatuservice.tranferidaPagos(datos[key]).length
      );
      this.filtroDias.transferidaRenovaciones.push(
        this.llamadasEstatuservice.tranferidaRenovaciones(datos[key]).length
      );
      this.filtroDias.transferidaSiniestros.push(
        this.llamadasEstatuservice.tranferidaSiniestros(datos[key]).length
      );
      this.totalLlamadasPorDia.push(this.datos[key].length);
    }
  }

  generaGrafico() {
    this.chartOptions = {
      series: this.datosGrafico,
      chart: {
        height: 350,
        type: 'line',
        stacked: false,
        toolbar: {
          tools: {
            download: false,
            selection: true,
            zoom: true,
            zoomin: true,
            zoomout: true,
            pan: true,
          },
        },
      },
      dataLabels: {
        enabled: true,
        enabledOnSeries: [9],
        formatter: function (val: number): any {
          return Intl.NumberFormat('en-US').format(val);
        },
      },
      stroke: {
        width: [1, 1, 1, 1, 3],
        curve: 'smooth',
      },
      markers: {
        size: 5,
      },
      title: {
        text: 'Total de Llamadas por Día y Estatus',
        align: 'center',
      },
      tooltip: {
        enabled: true,
        y: {
          formatter: function (val: number): any {
            return Intl.NumberFormat('en-US').format(val);
          },
        },
      },
      colors: this.colors,
      xaxis: {
        categories: this.xTitulos,
        title: {
          text: 'Fecha',
        },
      },
      yaxis: [
        {
          title: {
            text: 'Total de Llamadas',
          },
        },
      ],
      legend: {
        horizontalAlign: 'left',
        offsetX: 40,
        showForNullSeries: false,
        position: 'top',
      },
    };
  }
}
