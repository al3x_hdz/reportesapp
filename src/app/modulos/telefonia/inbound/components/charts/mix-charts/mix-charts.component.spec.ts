import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MixChartsComponent } from './mix-charts.component';

describe('MixChartsComponent', () => {
  let component: MixChartsComponent;
  let fixture: ComponentFixture<MixChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MixChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MixChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
