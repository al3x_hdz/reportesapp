import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-telefonia',
  templateUrl: './telefonia.component.html',
  styleUrls: ['./telefonia.component.css'],
})
export class TelefoniaComponent implements OnInit {
  loader: boolean;

  constructor() {}

  ngOnInit(): void {
    this.loader = true;
  }

  actualizaLoader(event: boolean) {
    this.loader = event;
  }
}
