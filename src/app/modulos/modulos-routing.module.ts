import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../@core/guards/auth.guard';

import { ModulosComponent } from './modulos.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    canActivate: [AuthGuard],
    path: '',
    component: ModulosComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'telefonia',
        loadChildren: () =>
          import('./telefonia/telefonia.module').then((m) => m.TelefoniaModule),
      },
      {
        path: 'venta-nueva',
        loadChildren: () =>
          import('./venta-nueva/venta-nueva.module').then(
            (m) => m.VentaNuevaModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModulosRoutingModule {}
