# App Reportería (Frontend)
Proyecto desarrollado por el equipo de Angular, ofrece las interfaces con las que va a interactuar el usuario,
Se conecta al backen por medio de servicios y peticiones con HttpClient.

## Documentación
Para ver la documentación detallada del proyecto click [aquí](https://drive.google.com/drive/folders/1q6qHoEZsMtWQQ6JVevQq44ZLZ5neJ_vg?usp=sharing)

### Proyectos relacionados
* [ReportesApp (backend)](https://bitbucket.org/al3x_hdz/microservicios-reportes/src/master/)
* [Tickets](https://bitbucket.org/trigarante/workspace/projects/TIC)

### Carpeta de poryecto completo en Bitbucket
* [Reportes](https://bitbucket.org/al3x_hdz/workspace/projects/REP)


## El proyecto cuenta con las siguientes características:


* [Angular 11](https://angular.io/)
* [Material](https://material.angular.io/)
* [Google login](https://developers.google.com/identity/sign-in/web/sign-in)
* [Apexcharts.js](https://apexcharts.com/)
* [Sweetalert2](https://sweetalert2.github.io/)
* [Xlsx](https://www.npmjs.com/package/xlsx)
* [moment.js](https://www.npmjs.com/package/moment)
* [bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/) V5

## Instrucciones de uso
1. Correo registrado en **Trigarante2020**, visible desde **usuarioView** columna **usuario**
2. Tener corriendo la app del [backend](https://bitbucket.org/al3x_hdz/microservicios-reportes/src/master/).
3. Configurar [enviorment](https://bitbucket.org/al3x_hdz/reportesapp/src/master/src/environments/environment.ts) que apunta al backend, el valor default es **http://localhost:3000**
4. dentro de la carpeta raíz correr el comando: ```ng serve```

## Vistas disponibles
* /login ---> Después de login, las rutas de la aplicación solo son accesibles una vez iniciada las sesión con Google Account.
* /modulos/dashboard ---> Vista principal de la aplicación, nos muestra un resumen y descripción de la App de Reportería.
* /modulos/venta-nueva ---> Vista con reporte de venta nueva, contiene 5 tabs con la siguiente información:
	* Venta Nueva
	* Gastos Médicos
	* Perú
	* Campañas
	* Campaña por Compañia
* /modulos/telefonia ---> Vista que muestra el reporte llamadas Inbound, contiene gráficos de llamadas de hoy(separadas por hora), llamadas del mes(totales, atendidas y no atendidas por ejecutivos) y llamadas del mes separadas por día.


